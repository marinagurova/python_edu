def flower_with_vals(flower="ромашка", color="белый", price=10.25):
    """
    Функция flower_with_vals.

    Принимает 3 аргумента:
    цветок (по умолчанию "ромашка"),
    цвет (по умолчанию "белый"),
    цена (по умолчанию 10.25).

    Функция flower_with_default_vals выводит строку в формате
    "Цветок: <цветок> | Цвет: <цвет> | Цена: <цена>".

    При этом в функции flower_with_default_vals есть проверка на то, что цветок и цвет - это строки

    (* Подсказка: type(x) == str или isinstance(s, str)), а также цена - это число больше 0, но меньше 10000.
    В функции main вызвать функцию flower_with_default_vals различными способами
    (перебрать варианты: цветок, цвет, цена, цветок цвет, цветок цена, цвет цена, цветок цвет цена).

    (* Использовать именованные аргументы).
    """
    if type(flower) == str and type(color) == str and 0 < int(price) < 10000:
        print("Цветок: {} | Цвет: {} | Цена: {}".format(flower, color, price))

if __name__ == "__main__":
    flower_with_vals("роза", "красный", 67)
    flower_with_vals(color="красный", flower="роза")
    flower_with_vals("роза")
    flower_with_vals(color="белый")
    flower_with_vals(price=900)
    flower_with_vals("роза", "белый")
    flower_with_vals("роза", price=88)
    flower_with_vals(color="красный", price=700)
