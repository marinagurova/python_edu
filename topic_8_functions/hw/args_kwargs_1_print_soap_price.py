def print_soap_price(soapname, *prices):
    """
    Функция print_soap_price.

    Принимает 2 аргумента: название мыла (строка) и неопределенное количество цен (*args).

    Функция print_soap_price выводит вначале название мыла, а потом все цены на него.

    В функции main вызывается функция print_soap_price и передается название мяла и произвольное количество цен.

    Пример: print_soap_price(‘Dove’, 10, 50) или print_soap_price(‘Мылко’, 456, 876, 555).
    """
    #вывод в строчку:
    #print(f"{soapname}" + str(prices))

    #вывод в столбик:
    print(f"{soapname}")
    for i in prices:
        print(i)

print_soap_price("Soap123", 67, 98, 100000, 456)