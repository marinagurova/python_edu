"""
Ввод данных с клавиатуры:
- Имя
- Фамилия
- Возраст

Вывод данных в формате:
"Вас зовут <Имя> <Фамилия>! Ваш возраст равен <Возраст>."
"""
#input:
my_name = input("Please, input your name: ")
my_surname = input("Please, input your surname: ")
my_age = input("Please, input your age: ")

#output:
print(f"Вас зовут {my_name} {my_surname}! Ваш возраст равен {my_age}.")
