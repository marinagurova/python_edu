def check_substr(x, y):
    """
    Функция check_substr.

    Принимает две строки.
    Если меньшая по длине строка содержится в большей, то возвращает True,
    иначе False.
    Если строки равны, то False.
    Если одна из строк пустая, то True.
    """
    if y in x and len(x) > len(y) or x in y and len(y) > len(x):
        return True
    elif x == y:
        return False
    elif x == '' or y == '':
        return True
    else:
        return False

if __name__ == '__main__':
    print(check_substr('qwe', 'w'))