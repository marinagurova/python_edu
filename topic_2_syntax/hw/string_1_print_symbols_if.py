def print_symbols_if(x_string):
    """
    Функция print_symbols_if.

    Принимает строку.

    Если строка нулевой длины, то вывести строку "Empty string!".

    Если длина строки больше 5, то вывести первые три символа и последние три символа.
    Пример: string='123456789' => result='123789'

    Иначе вывести первый символ столько раз, какова длина строки.
    Пример: string='345' => result='333'
    """
    if len(x_string) == 0:
        print('Empty string!')
    elif len(x_string) > 5:
        print(x_string[:3] + x_string[-3:])
    else:
        print(x_string[0] * len(x_string))

if __name__ == '__main__':
    print(print_symbols_if('4'))
