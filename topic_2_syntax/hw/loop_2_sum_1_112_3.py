def sum_1_112_3():
    """
    Функция sum_1_112_3.

    Вернуть сумму 1+4+7+10+...109+112.
    """
    row = []
    for i in range(1, 113, 3):
        row.append(i)
    return sum(row)

if __name__ == '__main__':
    print(sum_1_112_3())
