def count_odd_num(n):
    """
    Функция count_odd_num.

    Принимает натуральное число (целое число > 0).
    Верните количество нечетных цифр в этом числе.
    Если число равно 0, то вернуть "Must be > 0!".
    Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
    """
    flag = 0
    if type(n) != int:
        return 'Must be int!'
    elif n <= 0:
        return 'Must be > 0!'
    else:
        y = map(int, str(n))
        for i in y:
            if int(i) % 2 != 0:
                flag += 1
        return flag

if __name__ == '__main__':
    print(count_odd_num(4545))
