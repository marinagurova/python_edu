def check_sum(x1, x2, x3):
    """
    Функция check_sum.

    Принимает 3 числа.
    Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
    """
    if x1 + x2 == x3 or x1 + x3 == x2 or x2 + x3 == x1:
        return True
    else:
        return False


if __name__ != '__main__':
    print(check_sum(4, 6, 6))
