def arithmetic(x1, x2, action):
    """
    Функция arithmetic.

    Принимает 3 аргумента: первые 2 - числа, третий - операция, которая должна быть произведена над ними.
    Если третий аргумент +, сложить их;
    если —, то вычесть;
    если *, то умножить;
    если /, то разделить (первое на второе).
    В остальных случаях вернуть строку "Unknown operator".
    Вернуть результат операции.
    """
    if action == '+':
        return x1 + x2
    elif action == '-':
        return x1 - x2
    elif action == '*':
        return x1 * x2
    elif action == '/':
        if x2 == 0:
            return 'Division by zero!'
        else:
            return x1 / x2
    else:
        return 'Unknown operator'


if __name__ == '__main__':
    print(arithmetic(4, 5, '+'))
