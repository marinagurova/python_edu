"""
Функция triple_sting_print_len.

Принимает строку my_str.
Вернуть эту строку в таком виде: "<my_str>, <my_str>? <my_str>! len=<len(my_str)> :)".

Если строка пустая, то вернуть None
"""