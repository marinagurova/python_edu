def save_dict_to_file_classic(path_string, my_dict):
    """
    Функция save_dict_to_file_classic.

    Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

    Сохраняет список в файл. Проверить, что записалось в файл.
    """

    with open(path_string, 'w') as f:
        f.write(str(my_dict))

if __name__ == '__main__':
    save_dict_to_file_classic('file1.txt', {'sammy': 'cat'})
