def read_str_from_file(string_file):
    """
    Функция read_str_from_file.

    Принимает 1 аргумент: строка (название файла или полный путь к файлу).

    Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и наполнен каким-то текстом.)
    """
    with open(string_file, 'r') as file:
        print(file.read())

if __name__ == '__main__':
    read_str_from_file('file1.txt')
