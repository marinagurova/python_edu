import pickle

def save_dict_to_file_pickle(path_string, my_dict):
    """
    Функция save_dict_to_file_pickle.

    Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

    Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
    """
    with open(path_string, 'wb') as fp:
        pickle.dump(my_dict, fp)

    with open(path_string, 'rb') as f:
        text_from_file = pickle.load(f)
        print(text_from_file)

if __name__ == '__main__':
    save_dict_to_file_pickle('file_pickle.pkl', {'sammy': 'murmur'})
