def list_to_str(my_list, divided_string):
    """
    Функция list_to_str.

    Принимает 2 аргумента: список и разделитель (строка).

    Возвращает (строку полученную разделением элементов списка разделителем,
    количество разделителей в получившейся строке в квадрате).

    Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.

    Если разделитель не является строкой, то возвращать строку 'Second arg must be str!'.

    Если список пуст, то возвращать пустой tuple().

    ВНИМАНИЕ: в списке могут быть элементы любого типа (нужно конвертировать в строку).
    """
    if type(my_list) != list:
        return 'First arg must be list!'
    elif type(divided_string) != str:
        return 'Second arg must be str!'
    elif len(my_list) == 0:
        return tuple()
    else:
        my_list = [str(i) for i in my_list]
        string = divided_string.join(my_list)
        return (string, string.count(divided_string) ** 2)

if __name__ == '__main__':
    print(list_to_str([3, 87, 2, 9], '!'))
