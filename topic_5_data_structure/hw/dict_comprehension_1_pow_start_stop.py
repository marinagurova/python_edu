def pow_start_stop(start, stop):
    """
    Функция pow_start_stop.

    Принимает числа start, stop.

    Возвращает словарь, в котором
    ключ - это значение от start до stop (не включая)
    значение - это квадрат ключа.

    Пример: start=3, stop=6, результат {3: 9, 4: 16, 5: 25}.

    Если start или stop не являются int, то вернуть строку 'Start and Stop must be int!'.
    """
    if type(start) != int or type(stop) != int:
        return 'Start and Stop must be int!'
    return {i: i ** 2 for i in range(start, stop)}

if __name__ == '__main__':
    print(pow_start_stop(2, 50))