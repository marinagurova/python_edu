def get_translation_by_word(word_dict, word_ru):
    """
    Функция get_translation_by_word.

    Принимает 2 аргумента:
    ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
    слово для поиска в словаре (ru).

    Возвращает все варианты переводов (list), если такое слово есть в словаре,
    если нет, то ‘Can`t find Russian word: {word}’.

    Если вместо словаря передано что-то другое, то возвращать строку 'Dictionary must be dict!'.
    Если вместо строки для поиска передано что-то другое, то возвращать строку 'Word must be str!'.

    Если словарь пустой, то возвращать строку 'Dictionary is empty!'.
    Если строка для поиска пустая, то возвращать строку 'Word is empty!'.
    """
    if type(word_dict) != dict:
        return 'Dictionary must be dict!'
    elif type(word_ru) != str:
        return 'Word must be str!'
    elif not word_dict:
        return 'Dictionary is empty!'
    elif not word_ru:
        return 'Word is empty!'
    else:
        return word_dict.get(word_ru, f'Can`t find Russian word: {word_ru}')

if __name__ == '__main__':
    print(get_translation_by_word({'собачка': 'dog', 'котик': 'cat'}, 'собачка'))
