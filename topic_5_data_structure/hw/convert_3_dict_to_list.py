def dict_to_list(my_dict):
    """
    Функция dict_to_list.

    Принимает 1 аргумент: словарь.

    Возвращает кортеж: (
    список ключей,
    список значений,
    количество уникальных элементов в списке ключей,
    количество уникальных элементов в списке значений
    ).

    Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.
    """
    if type(my_dict) != dict:
        return 'Must be dict!'
    else:
        keys_list = list(my_dict.keys())
        val_list = list(my_dict.values())
        return (keys_list, val_list, len(set(keys_list)), len(set(val_list)))

if __name__ == '__main__':
    print(dict_to_list({1: 'der', 1: 'sd', 3: 'sammy'}))
