import itertools

def dict_pow2_start_stop_step_if_div3(start, stop, step):
    """
    Функция dict_pow2_start_stop_step_if_div3.

    Принимает 3 аргумента: числа start, stop, step.

    Возвращает генератор-выражение состоящий из кортежа (аналог dict):
    (значение, значение в квадрате)
    при этом значения перебираются от start до stop (не включая) с шагом step
    только для чисел, которые делятся на 3 без остатка.

    Пример: start=0, stop=10, step=1 результат ((0, 0), (3, 9), (6, 36), (9, 81)).

    Если start или stop или step не являются int, то вернуть строку 'Start and Stop and Step must be int!'.
    Если step равен 0, то вернуть строку "Step can't be zero!"
    """
    if not (type(start) == type(stop) == type(step) == int):
        return 'Start and Stop and Step must be int!'

    if step == 0:
        return 'Step can\'t be zero!'

    return ((x, x ** 2) for x in range(start, stop, step) if x % 3 == 0)


if __name__ == '__main__':
    print(dict_pow2_start_stop_step_if_div3(0, 10, 1))
