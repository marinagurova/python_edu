from itertools import zip_longest

def zip_car_year(cars, years):
    """
    Функция zip_car_year.

    Принимает 2 аргумента: список с машинами и список с годами производства.

    Возвращает список с парами значений из каждого аргумента, если один список больше другого,
    то заполнить недостающие элементы строкой "???".

    Подсказка: zip_longest.

    Если вместо списков передано что-то другое, то возвращать строку 'Must be list!'.
    Если список (хотя бы один) пуст, то возвращать строку 'Empty list!'.
    """
    if type(cars) != list or type(years) != list:
        return 'Must be list!'
    elif not cars or not years:
        return 'Empty list!'
    else:
        return list(zip_longest(cars, years, fillvalue = '???'))

if __name__ == '__main__':
    print(zip_car_year(['bmw', 'mers', 'honda'], ['1950', '1930', '1940', '1920']))