def zip_names(names, surnames):
    """
    Функция zip_names.

    Принимает 2 аргумента: список с именами и множество с фамилиями.

    Возвращает список с парами значений из каждого аргумента.

    Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
    Если вместо set передано что-то другое, то возвращать строку 'Second arg must be set!'.

    Если list пуст, то возвращать строку 'Empty list!'.
    Если set пуст, то возвращать строку 'Empty set!'.

    Если list и set различного размера, обрезаем до минимального (стандартный zip).
    """
    if type(names) != list:
        return 'First arg must be list!'
    elif type(surnames) != set:
        return 'Second arg must be set!'
    elif not names:
        return 'Empty list!'
    elif not surnames:
        return 'Empty set!'
    return list(zip(names, surnames))

if __name__ == '__main__':
    print(zip_names(['a', 'b', 'd'], {'d', 'e'}))
