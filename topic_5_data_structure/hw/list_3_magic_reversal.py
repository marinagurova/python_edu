def magic_reversal(my_list):
    """
    Функция magic_reversal.

    Принимает 1 аргумент: список my_list.

    Создает новый список new_list (копия my_list), порядок которого обратный my_list.

    Возвращает список, который состоит из
    [второго элемента (индекс=1) new_list]
    + [предпоследнего элемента new_list]
    + [весь new_list].

    Пример:
    входной список [1,  'aa', 99]
    new_list [99, 'aa', 1]
    результат ['aa', 'aa', 99, 'aa', 1].

    Если список состоит из одного элемента, то "предпоследний" = единственный и "второй элемент" = единственный.

    Если вместо списка передано что-то другое, то возвращать строку 'Must be list!'.
    Если список пуст, то возвращать строку 'Empty list!'.

    ВНИМАНИЕ: Изначальный список не должен быть изменен!
    """
    if type(my_list) != list:
        return 'Must be list!'
    elif len(my_list) == 0:
        return 'Empty list!'
    new_list = my_list.copy()
    return [list(reversed(new_list))[1 % len(new_list)]] + [list(reversed(new_list))[-2 % len(new_list)]] + list(reversed(new_list))

if __name__ == '__main__':
    print(magic_reversal([1, 2]))
