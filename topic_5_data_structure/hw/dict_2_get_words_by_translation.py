def get_words_by_translation(word_dict, word_eng):
    """
    Функция get_words_by_translation.

    Принимает 2 аргумента:
    ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
    слово для поиска в словаре (eng).

    Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
    если нет, то ‘Can`t find English word: {word}’.

    Если вместо словаря передано что-то другое, то возвращать строку 'Dictionary must be dict!'.
    Если вместо строки для поиска передано что-то другое, то возвращать строку 'Word must be str!'.

    Если словарь пустой, то возвращать строку 'Dictionary is empty!'.
    Если строка для поиска пустая, то возвращать строку 'Word is empty!'.
    """
    if type(word_dict) != dict:
        return 'Dictionary must be dict!'
    elif type(word_eng) != str:
        return 'Word must be str!'
    elif not word_dict:
        return 'Dictionary is empty!'
    elif not word_eng:
        return 'Word is empty!'

    word_ru_list = []
    for word_ru, word_eng_list in word_dict.items():
        if word_eng in word_eng_list:
            word_ru_list.append(word_ru)

    if len(word_ru_list) == 0:
        return f"Can`t find English word: {word_eng}"

    return word_ru_list



if __name__ == '__main__':
    print(get_words_by_translation({'собачка': ['dog', 'god'], 'котик': 'cat'}, 'dog'))
