def zip_colour_shape(colours, shapes):
    """
    Функция zip_colour_shape.

    Принимает 2 аргумента: список с цветами(зеленый, красный) и кортеж с формами (квадрат, круг).

    Возвращает список с парами значений из каждого аргумента.

    Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
    Если вместо tuple передано что-то другое, то возвращать строку 'Second arg must be tuple!'.

    Если list пуст, то возвращать строку 'Empty list!'.
    Если tuple пуст, то возвращать строку 'Empty tuple!'.

    Если list и tuple различного размера, обрезаем до минимального (стандартный zip).
    """
    if type(colours) != list:
        return 'First arg must be list!'
    elif type(shapes) != tuple:
        return 'Second arg must be tuple!'
    elif not colours:
        return 'Empty list!'
    elif not shapes:
        return 'Empty tuple!'
    else:
        return list(zip(colours, shapes))

if __name__ == '__main__':
    print(zip_colour_shape(['red', 'green', 'blue', 'red'], ('circle', 'square', 'point')))
