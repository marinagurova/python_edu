import random
from game_sp.source_code.hero_npc import HeroNPC
from game_sp.source_code.hero_state import HeroState
from game_sp.source_code.hero_type import HeroType
from game_sp.source_code.hero_type_weaknesses import hero_weakness

class TestNPCClass:
    hero_name = 'Stan'
    hero_type = HeroType.WARRIOR
    max_hp = 50

    def setup_method(self, method):
        random.seed(123)

    def test_init(self):
        test_hero = HeroNPC()

        assert test_hero.name == TestNPCClass.hero_name
        assert test_hero.hero_type == TestNPCClass.hero_type
        assert test_hero.weaknesses == hero_weakness[TestNPCClass.hero_type]
        assert test_hero.hp == TestNPCClass.max_hp
        assert test_hero.attack_point is None
        assert test_hero.defence_point is None
        assert test_hero.hit_power == 5
        assert test_hero.state == HeroState.READY

    def test_str(self):
        test_hero =HeroNPC()
        assert str(test_hero) == f"Name: {TestNPCClass.hero_name} | Type: {TestNPCClass.hero_type}\nHP: {TestNPCClass.max_hp}"
