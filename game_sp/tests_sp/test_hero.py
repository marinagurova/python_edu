from game_sp.source_code.hero import Hero
from game_sp.source_code.hero_state import HeroState
from game_sp.source_code.hero_type import HeroType
from game_sp.source_code import hero_type_weaknesses


class TestHeroClass:
    hero_name = 'Cartman'
    hero_type = HeroType.THIEF
    max_hp = 50

    def test_init(self, ):
        test_hero = Hero(name=self.__class__.hero_name, type=self.__class__.hero_type, max_hp=self.__class__.max_hp)

        assert test_hero.name == TestHeroClass.hero_name
        assert test_hero.hero_type == TestHeroClass.hero_type
        assert test_hero.weaknesses == hero_type_weaknesses.hero_weakness[TestHeroClass.hero_type]
        assert test_hero.hp == TestHeroClass.max_hp
        assert test_hero.attack_point is None
        assert test_hero.defence_point is None
        assert test_hero.hit_power == 5
        assert test_hero.state == HeroState.READY

    def test_str(self):
        test_hero = Hero(name=self.__class__.hero_name, type=self.__class__.hero_type, max_hp=self.__class__.max_hp)

        assert str(
            test_hero) == f"Name: {TestHeroClass.hero_name} | Type: {TestHeroClass.hero_type}\nHP: {TestHeroClass.max_hp}"
