from enum import Enum, auto

class HeroType(Enum):
    THIEF = auto()
    WARRIOR = auto()
    WIZARD = auto()
    JEW = auto()

    @classmethod
    def min_value(cls):
        return cls.THIEF.value

    @classmethod
    def max_value(cls):
        return cls.JEW.value
