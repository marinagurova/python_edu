from enum import Enum, auto

class GameResult(Enum):
    WIN = auto()
    LOST = auto()
    DRAW = auto()
