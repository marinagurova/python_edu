import random

from hero import Hero
from hero_type import HeroType
from hero_by_type import hero_by_type
from impacts import Impact


class HeroNPC(Hero):
    def __init__(self):
        rand_type_value = random.randint(HeroType.min_value(), HeroType.max_value())
        rand_hero_type = HeroType(rand_type_value)
        rand_hero_name = random.choice(list(hero_by_type.get(rand_hero_type, {}).keys()))
        super().__init__(rand_hero_name, rand_hero_type)

    def next_step_points(self):
        attack_point = Impact(random.randint(Impact.min_value(), Impact.max_value()))
        defence_point = Impact(random.randint(Impact.min_value(), Impact.max_value()))
        super().next_step_points(next_attack=attack_point, next_defence=defence_point)


if __name__ == '__main__':
    hero_npc = HeroNPC()
    hero_npc.next_step_points()
    print(hero_npc)