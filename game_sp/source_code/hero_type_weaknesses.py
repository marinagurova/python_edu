from hero_type import HeroType

hero_weakness = {
    HeroType.THIEF: (HeroType.WARRIOR, HeroType.WIZARD),
    HeroType.WARRIOR: (HeroType.WIZARD, HeroType.JEW),
    HeroType.WIZARD: (HeroType.JEW,),
    HeroType.JEW: (HeroType.THIEF,)
}