from hero_type import HeroType
from hero_type_weaknesses import hero_weakness
from impacts import Impact
from hero_state import HeroState


class Hero:
    def __init__(self, name: str, hero_type: HeroType):
        self.name = name
        self.hero_type = hero_type
        self.weaknesses = hero_weakness.get(hero_type, tuple())
        self.hp = 50
        self.attack_point = None
        self.defence_point = None
        self.hit_power = 5
        self.state = HeroState.READY

    def __str__(self):
        return f"Name: {self.name} | Type: {self.hero_type.name}\nHP: {self.hp}"

    def next_step_points(self, next_attack: Impact, next_defence: Impact):
        self.attack_point = next_attack
        self.defence_point = next_defence

    def get_hit(self, opponent_attack_point: Impact, opponent_hit_power: int, opponent_type: HeroType):
        if opponent_attack_point == Impact.NOTHING:
            return "Мимо!"
        elif self.defence_point == opponent_attack_point:
            return "Буль буль"
        else:
            self.hp -= opponent_hit_power * (3 if opponent_type in self.weaknesses else 1)

            if self.hp <= 0:
                self.state = HeroState.DEFEATED
                return "Screw you Guys, I'm going home!"
            else:
                return "Не страшно проиграть, но если проиграешь — будешь наказан!"


if __name__ == '__main__':
    p1 = Hero('ololol', HeroType.WIZARD)
    print(p1)
