from hero_type import HeroType

hero_by_type = {
    HeroType.WARRIOR: {
        "Stan": "stan.jpg",
        "ManBearPig": "manbearpig.jpg"
    },
    HeroType.WIZARD: {
        "Randy": "randy.jpg",
        "Mr Garrison": "mr_garrison.jpg",
        "Satan": "satan.jpg"
    },
    HeroType.THIEF: {
        "Cartman": "cartman.jpg",
        "Kenny": "kenny.jpg",
        "Butters": "butters.jpg"
    },
    HeroType.JEW: {
        "Kyle": "kyle.jpg",
        "Jesus": "jesus.jpg"
    }
}
