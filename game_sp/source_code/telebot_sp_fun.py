import telebot
from telebot import types
from telebot.types import Message

from hero import Hero
from hero_npc import HeroNPC
from hero_by_type import hero_by_type
from hero_type import HeroType
from impacts import Impact
from game_result import GameResult
from hero_state import HeroState
import json


with open("./token.txt", 'r') as token_file:
    token = token_file.read().strip()

bot = telebot.TeleBot(token)

state = {}

statistics = {}
stat_file = "game_stat.json"


def load_stat():
    print('Загрузка статистики...', end='')
    global statistics

    try:
        with open(stat_file, 'r') as file:
            statistics = json.load(file)
        print('завершена успешно!')
    except FileNotFoundError as my_error:
        statistics = {}
        print('файл не найден!')


def update_save_stat(user_id, result: GameResult):
    print("Обновление статистики", end="...")
    global statistics

    if statistics.get(user_id, None) is None:
        statistics[str(user_id)] = {}

    if result == GameResult.WIN:
        statistics[str(user_id)]['WIN'] = statistics[str(user_id)].get('WIN', 0) + 1
    elif result == GameResult.LOST:
        statistics[str(user_id)]['LOST'] = statistics[str(user_id)].get('LOST', 0) + 1
    elif result == GameResult.DRAW:
        statistics[str(user_id)]['DRAW'] = statistics[str(user_id)].get('DRAW', 0) + 1
    else:
        print(f"Не существует результата {result}")

    with open(stat_file, 'w') as file:
        json.dump(statistics, file)



@bot.message_handler(commands=["help", "info"])
def help_command(message):
    bot.send_message(message.chat.id,
                     "Привет! Это простая игра: Кенни попал на небеса и теперь возглавит армию рая, но войска Сатаны не отступают."
                     "Выбери себе персонажа, бей и защищайся. Проиграет тот, у кого hp обнулится."
                     "Теперь надо ввести /start. Для вывода статистики ввести /stat")


@bot.message_handler(commands=["start"])
def start(message):
    yes_no_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True, row_width=2)
    yes_no_keyboard.row("Да", "Нет")

    bot.send_message(message.from_user.id, text="Прислужник сатаны: Начинайте наступление.",
                     reply_markup=yes_no_keyboard)

    bot.register_next_step_handler(message, start_question_handler)


@bot.message_handler(commands=["stat"])
def stat(message):
    global statistics
    if statistics.get(str(message.chat.id), None) is None:
        user_stat = "Еще не было ни одной игры"
    else:
        user_stat = "Твои результаты:"
        for res, num in statistics[str(message.chat.id)].items():
            user_stat += f"\n{res}: {num}"

    bot.send_message(message.chat.id,
                     text=user_stat)


def start_question_handler(message):
    if message.text.lower() == 'да':
        bot.send_message(message.from_user.id, "Let's go!")

        create_npc(message)

        ask_user_about_hero_type(message)

    elif message.text.lower() == 'нет':
        bot.send_message(message.from_user.id, 'Расходимся.')
    else:
        bot.send_message(message.from_user.id, 'Я не знаю такого варианта ответа!')


def create_npc(message):
    print(f"Начало создания объекта NPC для chat id = {message.chat.id}")
    global state
    hero_npc = HeroNPC()
    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['npc_hero'] = hero_npc

    npc_image_filename = hero_by_type[hero_npc.hero_type][hero_npc.name]
    bot.send_message(message.chat.id, 'Противник:')
    with open(f"../images/{npc_image_filename}", 'rb') as file:
        bot.send_photo(message.chat.id, file, hero_npc)
    print(f"Завершено создание объекта NPC для chat id = {message.chat.id}")


def ask_user_about_hero_type(message):
    markup = types.InlineKeyboardMarkup()

    for hero_type in HeroType:
        markup.add(types.InlineKeyboardButton(text=hero_type.name, callback_data=f"hero_type_{hero_type.value}"))

    bot.send_message(message.chat.id, "Выбери тип себя:", reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "hero_type_" in call.data)
def hero_type_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) < 3 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, "Возникла проблема. Перезапусти сессию!")
    else:
        hero_type_id = int(call_data_split[2])

        bot.send_message(call.message.chat.id, "Выбери кто ты из жителей Южного Парка:")

        ask_user_about_hero_by_type(hero_type_id, call.message)


def ask_user_about_hero_by_type(hero_type_id, message):
    hero_type = HeroType(hero_type_id)
    hero_dict_by_type = hero_by_type.get(hero_type, {})

    for hero_name, hero_img in hero_dict_by_type.items():
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton(text=hero_name, callback_data=f"hero_name_{hero_type_id}_{hero_name}"))
        with open(f"../images/{hero_img}", 'rb') as file:
            bot.send_photo(message.chat.id, file, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "hero_name_" in call.data)
def hero_name_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) < 4 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, "Возникла проблема. Перезапусти сессию!")
    else:
        hero_type_id, hero_name = int(call_data_split[2]), call_data_split[3]

        create_user_hero(call.message, hero_type_id, hero_name)

        bot.send_message(call.message.chat.id, "Игра началась!")

        game_next_step(call.message)


def create_user_hero(message, hero_type_id, hero_name):
    global state
    user_hero = Hero(name=hero_name, hero_type=HeroType(hero_type_id))

    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['user_hero'] = user_hero

    image_filename = hero_by_type[user_hero.hero_type][user_hero.name]
    bot.send_message(message.chat.id, 'Твой выбор:')
    with open(f"images/{image_filename}", 'rb') as file:
        bot.send_photo(message.chat.id, file, user_hero)


impact_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                            one_time_keyboard=True,
                                            row_width=len(Impact))

impact_keyboard.row(*[types.KeyboardButton(i.name) for i in Impact])


def game_next_step(message: Message):
    bot.send_message(message.chat.id,
                     "Выбор точки для защиты:",
                     reply_markup=impact_keyboard)

    bot.register_next_step_handler(message, reply_defend)


def reply_defend(message: Message):
    if not Impact.has_item(message.text):
        bot.send_message(message.chat.id, "Необходимо выбрать вариант на клавиатуре!")
        game_next_step(message)
    else:
        bot.send_message(message.chat.id,
                         "Выбор точки для удара:",
                         reply_markup=impact_keyboard)

        bot.register_next_step_handler(message, reply_attack, defend_impact=message.text)


def reply_attack(message: Message, defend_impact: str):
    if not Impact.has_item(message.text):
        bot.send_message(message.chat.id, "Необходимо выбрать вариант на клавиатуре!")
        game_next_step(message)
    else:
        attack_impact = message.text

        global state
        # обращение по ключу ко вложенному словарю для получения объекта персонажа пользователя
        user_hero = state[message.chat.id]['user_hero']

        # обращение по ключу ко вложенному словарю для получения объекта персонажа npc
        hero_npc = state[message.chat.id]['npc_hero']

        # конвертировать строку хранящуюся в объекте attack_impact в правильный тип enum Impact
        # конвертировать строку хранящуюся в объекте defend_impact в правильный тип enum Impact
        user_hero.next_step_points(next_attack=Impact[attack_impact],
                                   next_defence=Impact[defend_impact])

        hero_npc.next_step_points()

        game_step(message, user_hero, hero_npc)


def game_step(message: Message, user_hero: Hero, hero_npc: Hero):
    comment_npc = hero_npc.get_hit(opponent_attack_point=user_hero.attack_point,
                                   opponent_hit_power=user_hero.hit_power,
                                   opponent_type=user_hero.hero_type)
    bot.send_message(message.chat.id, f"NPC hero: {comment_npc}\nHP: {hero_npc.hp}")

    comment_user = user_hero.get_hit(opponent_attack_point=hero_npc.attack_point,
                                     opponent_hit_power=hero_npc.hit_power,
                                     opponent_type=hero_npc.hero_type)
    bot.send_message(message.chat.id, f"Your hero: {comment_user}\nHP: {user_hero.hp}")

    if hero_npc.state == HeroState.READY and user_hero.state == HeroState.READY:
        bot.send_message(message.chat.id, "Продолжаем!")
        game_next_step(message)
    elif hero_npc.state == HeroState.DEFEATED and user_hero.state == HeroState.DEFEATED:
        bot.send_message(message.chat.id, "Ничья!")
        update_save_stat(message.chat.id, GameResult.DRAW)
    elif hero_npc.state == HeroState.DEFEATED:
        bot.send_message(message.chat.id, "Ты победил :)")
        update_save_stat(message.chat.id, GameResult.WIN)
    elif user_hero.state == HeroState.DEFEATED:
        bot.send_message(message.chat.id, "Ты проиграл :)")
        update_save_stat(message.chat.id, GameResult.LOST)


if __name__ == '__main__':
    load_stat()
    print('Starting the bot...')
    bot.polling(none_stop=True, interval=0)
    print('The bot has stopped!')
