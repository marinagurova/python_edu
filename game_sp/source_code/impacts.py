from enum import Enum, auto


class Impact(Enum):
    NOTHING = auto()
    WALLET = auto()
    BODY = auto()
    MIND = auto()

    @classmethod
    def min_value(cls):
        return cls.NOTHING.value

    @classmethod
    def max_value(cls):
        return cls.MIND.value

    @classmethod
    def has_item(cls, name):
        return name in cls._member_names_
