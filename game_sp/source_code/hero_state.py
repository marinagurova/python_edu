from enum import Enum, auto

class HeroState(Enum):
    READY = auto()
    DEFEATED = auto()
