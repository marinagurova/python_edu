from topic_7_oop.practice.class_2_0_organ import Organ


class Eye(Organ):
    def __init__(self, healthy: bool, mass: int, visual_acuity: str, colour: str):
        super().__init__(healthy, mass)
        self.visual_acuity = visual_acuity
        self.colour = colour

    def listen_to(self):
        print("hlop-hlop-hlop")
