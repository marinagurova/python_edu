class Pupil:
    """
    Класс Pupil.

    Поля:
    имя: name,
    возраст: age,
    dict с оценками: marks (пример: {'math': [3, 5], 'english': [5, 5, 4] ...].

    Методы:
    get_all_marks: получить список всех оценок,
    get_avg_mark_by_subject: получить средний балл по предмету (если предмета не существует, то вернуть 0),
    get_avg_mark: получить средний балл (все предметы),
    __le__: вернуть результат сравнения (<=) средних баллов (все предметы) двух учеников.
    """
    def __init__(self, name, age, marks):
        self.name = name
        self.age = age
        self.marks = marks

    def get_all_marks(self):
        marks_list = [*self.marks.values()]
        all_marks_list = []
        for i in marks_list:
            all_marks_list.extend(i)
        return all_marks_list

    def get_avg_mark_by_subject(self, subject):
        if self.marks.get(subject):
            marks_list_by_subject = self.marks.get(subject)
            return sum(marks_list_by_subject) / len(marks_list_by_subject)
        else:
            return 0

    def get_avg_mark(self):
        return sum(self.get_all_marks()) / len(self.get_all_marks())

    def __le__(self, other):
        return self.get_avg_mark() <= other.get_avg_mark()
