from topic_7_oop.hw.class_3_1_chicken import Chicken
from topic_7_oop.hw.class_3_2_goat import Goat

class Farm:
    """
    Класс Farm.

    Поля:
    животные (list из произвольного количества Goat и Chicken): animals
    (вначале список пуст, потом добавялем животных методами append и extend самостоятельно),
    наименование фермы: name,
    имя владельца фермы: owner.

    Методы:
    get_goat_count: вернуть количество коз на ферме (*Подсказка isinstance или type == Goat),
    get_chicken_count: вернуть количество куриц на ферме,
    get_animals_count: вернуть количество животных на ферме,
    get_milk_count: вернуть сколько молока можно получить в день,
    get_eggs_count: вернуть сколько яиц можно получить в день.
    """
    def __init__(self, name, owner):
        self.zoo_animals = []
        self.name = name
        self.owner = owner

    def get_goat_count(self):
        #self.zoo_animals.extend([Goat(name, age, milk) for name, age, milk in self.zoo_animals])
        count_goats = 0
        for i in self.zoo_animals:
            if type(i) == Goat:
                count_goats += 1
        return count_goats

    def get_chicken_count(self):
        #self.zoo_animals.extend([Chicken(name, num, eggs) for name, num, eggs in self.zoo_animals])
        count_chicken = 0
        for i in self.zoo_animals:
            if type(i) == Chicken:
                count_chicken += 1
        return count_chicken

    def get_animals_count(self):
        return len(self.zoo_animals)

    def get_milk_count(self):
        milk_amount = sum([i.milk_per_day for i in self.zoo_animals if type(i) == Goat])
        return milk_amount

    def get_eggs_count(self):
        eggs_count = sum([i.eggs_per_day for i in self.zoo_animals if type(i) == Chicken])
        return eggs_count
