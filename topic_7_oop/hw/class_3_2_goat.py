class Goat:
    """
    Класс Goat.

    Поля:
    имя: name,
    возраст: age,
    сколько молока дает в день: milk_per_day.

    Методы:
    get_sound: вернуть строку 'Бе-бе-бе',
    __invert__: реверс строки с именем (например, была Маруся, а стала Ясурам). вернуть self
    __mul__: умножить milk_per_day на число. вернуть self
    """
    def __init__(self, name, age, milk):
        self.name = name
        self.age = age
        self.milk_per_day = milk

    def get_sound(self):
        return "Бе-бе-бе"

    def __invert__(self):
        self.name = self.name[::-1].capitalize()
        return self

    def __mul__(self, other):
        self.milk_per_day = self.milk_per_day * int(other)
        return self

if __name__ == '__main__':
    o = Goat("Маня", 3, 10)
    print(~o)
