'''
Выведите таблицу размером n×n, заполненную числами от 1 до n^2 по спирали,
выходящей из левого верхнего угла и закрученной по часовой стрелке, как показано в примере (здесь n=5):

Sample Input:
5

Sample Output:
1 2 3 4 5
16 17 18 19 6
15 24 25 20 7
14 23 22 21 8
13 12 11 10 9
'''
#ввод размера матрицы:
n = int(input())

#заполнение матрицы по умолчанию нулями:
matrix = [[0 for j in range(n)] for i in range(n)]

#counter_cycle - счетчик циклов спирали, т.е. на каком квадрате матрицы находимся, сколько отступов по сторонам
#counter_side - счетчик заполнения элементов по спирали
counter_cycle = 0
counter_side = 1

#первый цикл for определяет на каком квадрате матрицы находимся (от внешнего к внутренним, пока не дойдем до середины), n//2 - количество квадратов начиная с нуля.
#после прохождения цикла счетчик увеличивается на 2, т.к. на новом внутреннем квадрате уберется по одной позиции с каждой стороны
for cycle in range(n//2 + 1):
    #заполнение верхней стороны квадрата слева направо:
    for side in range(n - counter_cycle):
        matrix[cycle][cycle + side] = counter_side
        counter_side += 1
    #заполнение крайней правой стороны квадрата сверху вниз:
    for side in range(cycle + 1, n - cycle):
        matrix[side][n - (cycle + 1)] = counter_side
        counter_side += 1
    #заполнение нижней стороны квадрата справа налево:
    for side in range(cycle + 1, n - cycle):
        matrix[n - (cycle + 1)][-side - 1] = counter_side
        counter_side += 1
    #заполнение крайней левой стороны квадрата снизу вверх:
    for side in range(cycle + 1, n - (cycle + 1)):
        matrix[-side - 1][cycle] = counter_side
        counter_side += 1
    counter_cycle += 2

#вывод заполненной матрицы:
for i in matrix:
    print(*i)
