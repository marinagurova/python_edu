#ввод матрицы, происходит, пока не введут end:
string = input()
matrix = []
while string != 'end':
    matrix.append([int(i) for i in string.split()])
    string = input()

#zip склеивает элементы вложенных списков по каждому индексу: по нулевому будет 1, 4, 7, по первому 2, 3, 4 и т.д.
transposed_matrix = list(zip(*matrix))
#или list(zip(*matrix))

#вывод транспонированной матрицы
for i in range(len(transposed_matrix)):
    for j in range(len(transposed_matrix[i])):
        print(transposed_matrix[i][j], end = ' ')
    print()
