#ввод матрицы:
matrix = [[int(j) for j in input().split()] for i in range(3)]
#допустим, размер 3 на 3: matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

#zip склеивает элементы вложенных списков по каждому индексу: по нулевому будет 1, 4, 7, по первому 2, 3, 4 и т.д.
transposed_matrix = list(zip(matrix[0], matrix[1], matrix[2]))
#или list(zip(*matrix))

#вывод транспонированной матрицы
for i in range(len(transposed_matrix)):
    for j in range(len(transposed_matrix[i])):
        print(transposed_matrix[i][j], end = ' ')
    print()
